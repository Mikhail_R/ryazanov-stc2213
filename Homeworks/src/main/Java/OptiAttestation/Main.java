package OptiAttestation;

public class Main {
    public static void main(String[] args) {

        UsersRepositoryFile usersRepositoryFile = new UsersRepositoryFile();

        usersRepositoryFile.create(new User(5533,"Pavel", "Pavlov", 81, true));
        usersRepositoryFile.create(new User(7686,"Vladimir", "Vladimirov", 46, false));
        usersRepositoryFile.create(new User(9545,"Alexandr", "Alexandrov", 26, false));

        usersRepositoryFile.findByld(5533);

        User user = new User(5533, "Da", "Pavlov", 81, true);

        usersRepositoryFile.update(user);

        usersRepositoryFile.delete(5533);
    }
}
