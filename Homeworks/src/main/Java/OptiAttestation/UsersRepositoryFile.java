package OptiAttestation;

import java.io.*;

public class UsersRepositoryFile extends Service implements Userable {

    @Override
    public User findByld(int id) {
        readOurFile("OptiAttestation.txt");
        System.out.println("Пользователь по запросу ID найден:" + "\n" + userMap.get(id));
        return userMap.get(id);
    }


    @Override
    public void create(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("OptiAttestation.txt", true))) {
            writer.write(user.toString());
            writer.write("\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void update(User user) {
        readOurFile("OptiAttestation.txt");
        userMap.put(user.getId(), user);
        writeOurFile("OptiAttestation.txt");
        System.out.println(userMap.get(user.getId()) + " - Пользователь добавлен/изменён");
    }

    @Override
    public void delete(int id) {
        readOurFile("OptiAttestation.txt");
        userMap.remove(id);
        writeOurFile("OptiAttestation.txt");
        System.out.println(id + " - Пользователь удалён");
    }
}
