package OptiAttestation;

public interface Userable {

    User findByld(int id);

    void create(User user);

    void update(User user);

    void delete(int id);
}
