package OptiAttestation;

import java.util.Objects;

public class User {
    private int id;
    private String name;
    private String lastName;
    private int age;
    private boolean haveAJob;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getHaveAJob() {
        return haveAJob;
    }

    public void setHaveAJob(boolean haveAJob) {
        this.haveAJob = haveAJob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id == user.id && age == user.age && haveAJob == user.haveAJob && name.equals(user.name) && lastName.equals(user.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, age, haveAJob);
    }

    public User(int id, String name, String lastName, int age, boolean haveAJob) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.haveAJob = haveAJob;
    }

    public User() {
    }

    @Override
    public String toString() {
        return id +
                "|" + name +
                "|" + lastName +
                "|" + age +
                "|" + haveAJob;
    }

}
