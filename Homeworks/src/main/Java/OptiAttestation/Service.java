package OptiAttestation;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Service {

    String line = null;

    User user = new User();
    Map<Integer, User> userMap = new HashMap<>();

    public void readOurFile(String filename) {

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            while ((line = reader.readLine()) != null) {
                String[] readUserInFile = line.split("\\|");
                userMap.put(Integer.parseInt(readUserInFile[0]),
                        new User(Integer.parseInt(readUserInFile[0]),
                                readUserInFile[1],
                                readUserInFile[2],
                                Integer.parseInt(readUserInFile[3]),
                                Boolean.parseBoolean(readUserInFile[4])));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeOurFile(String filename) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, false))) {
            userMap.forEach((key, value) -> {
                try {
                    writer.write(userMap.get(key).toString());
                    writer.write("\n");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException o) {
            throw new RuntimeException();
        }
    }
}

