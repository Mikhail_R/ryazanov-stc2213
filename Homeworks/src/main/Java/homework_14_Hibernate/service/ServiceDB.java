package homework_14_Hibernate.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.cfg.Configuration;

public class ServiceDB {
    public EntityManagerFactory connectDB() {
        return new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
    }

    public EntityManager entityManager() {
        return connectDB().createEntityManager();
    }
}
