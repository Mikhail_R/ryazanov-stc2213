package homework_14_Hibernate;

import homework_14_Hibernate.model.Lector;
import homework_14_Hibernate.model.Student;
import homework_14_Hibernate.service.ServiceDB;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Main {
    public static void main(String[] args) {

//        ServiceDB serviceDB = new ServiceDB();
        EntityManagerFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();

        Student student = new Student("Mike", "Mikov");
        Student student1 = new Student("Nike", "Nikov");
        Student student2 = new Student("Spike", "Spikov");
        Lector lector = new Lector("Vasserman", "Math");
        Lector lector1 = new Lector("Bethoven", "Music");
        Lector lector2 = new Lector("Bah", "Music");

        student.setLectors(List.of(lector, lector1, lector2));
        student1.setLectors(List.of(lector, lector1, lector2));
        student2.setLectors(List.of(lector, lector1, lector2));
        lector.setStudents(List.of(student, student1, student2));
        lector1.setStudents(List.of(student, student1, student2));
        lector2.setStudents(List.of(student, student1, student2));

        em.getTransaction().begin();
        em.persist(student);
        em.persist(student1);
        em.persist(student2);
        em.persist(lector);
        em.persist(lector1);
        em.persist(lector2);
        em.getTransaction().commit();

    }
}
