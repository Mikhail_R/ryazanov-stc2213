package homework_10;


import java.util.Arrays;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int[] twoArray = new int[array.length];
        int digit = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                twoArray[digit] = array[i];
                digit++;
            }
        }
        int[] threeArr = new int[digit];
        System.arraycopy(twoArray, 0, threeArr, 0, digit);
        return threeArr;
    }
}

