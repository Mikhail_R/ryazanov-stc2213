package homework_10;
@FunctionalInterface
public interface ByCondition {

    boolean isOk(int number);
}
