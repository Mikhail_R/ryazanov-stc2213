package homework_10;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random rnd = new Random();
        int[] arr = new int[rnd.nextInt(100)];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rnd.nextInt((1000) + 1);
        }

        ByCondition condEven = number -> number % 2 == 0;

        ByCondition condSumEven = number -> {
            int sum = 0;
            while (number != 0) {
                sum += number % 10;
                number /= 10;
            }
            return sum % 2 == 0;
        };

        ByCondition condAllEven = number -> {
            boolean Digit = true;
            while (number != 0) {
                if ((number % 10) % 2 != 0) {
                    Digit = false;
                    break;
                }
                number /= 10;
            }
            return Digit;
        };

        ByCondition condPalindromeDig = number -> {
            boolean palindromeNum = true;
            String numStroke = String.valueOf(number);
            char[] symbolStr = numStroke.toCharArray();
            for (int i = 0; i < symbolStr.length; i++) {
                if (symbolStr[i] != symbolStr[symbolStr.length - i - 1]) {
                    palindromeNum = false;
                    break;
                }
            }
            return palindromeNum;

        };

        System.out.println("Начальный массив - " + Arrays.toString(arr));

        int[] filter = Sequence.filter(arr, condEven);
        System.out.println("Массив из чётных чисел - " + Arrays.toString(filter));

        int[] filter1 = Sequence.filter(arr, condSumEven);
        System.out.println("Массив, когда сумма цифр элемента - чётное число - " + Arrays.toString(filter1));

        int[] filter2 = Sequence.filter(arr, condAllEven);
        System.out.println("Массив, когда все цифры чётные - " + Arrays.toString(filter2));

        int[] filter3 = Sequence.filter(arr, condPalindromeDig);
        System.out.println("Массив, когда число палиндром - " + Arrays.toString(filter3));
    }
}
