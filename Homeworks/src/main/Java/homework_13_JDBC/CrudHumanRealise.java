package homework_13_JDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static homework_13_JDBC.ConnectToBD.connection;

public class CrudHumanRealise implements CrudHuman {

    private static final String GETBYID = "SELECT * FROM сitizens WHERE id = ?";
    private static final String DELETEHUMAN = "DELETE FROM сitizens WHERE id = ?";
    private static final String GETALL = "SELECT * FROM сitizens";
    private static final String UPDATEHUMAN = "UPDATE сitizens SET namehuman = ?, " +
            "lastname = ?, " +
            "patronymic = ?, " +
            "city = ?, " +
            "street = ?, " +
            "house = ?, " +
            "flat = ?, " +
            "numberpassport = ?" +
            "WHERE id = ?";
    private static final String CREATEHUMAN = "INSERT INTO сitizens (namehuman, " +
            "lastname, " +
            "patronymic, " +
            "city, " +
            "street, " +
            "house, " +
            "flat, " +
            "numberpassport) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    PreparedStatement preparedStatement = null;
    List<Human> humanList = new ArrayList<>();
    Human human = new Human();

    @Override
    public List<Human> getAllHumans() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GETALL);
            while (resultSet.next()) {
                humanList.add(new Human(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getString(8),
                        resultSet.getString(9)));
            }
            humanList.forEach(System.out::println);
        } catch (SQLException e) {
            System.out.println("Can't found table");
        }
        return humanList;
    }

    private Human convertResultSet(ResultSet resultSet) throws SQLException {
        Human human = null;
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String name = resultSet.getString(2);
            String lastName = resultSet.getString(3);
            String patronymic = resultSet.getString(4);
            String city = resultSet.getString(5);
            String street = resultSet.getString(6);
            String house = resultSet.getString(7);
            String flat = resultSet.getString(8);
            String numberPassport = resultSet.getString(9);
            human = new Human(id, name, lastName, patronymic, city, street, house, flat, numberPassport);
        }
        return human;
    }

    @Override
    public Human getHumanById(int id) {
        try {
            preparedStatement = connection.prepareStatement(GETBYID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            Human human = convertResultSet(resultSet);
            System.out.println(human.toString());
        } catch (SQLException e) {
            System.out.println("Not found user");
        }
        return human;
    }

    @Override
    public void createHuman(Human human) {
        try {
            preparedStatement = connection.prepareStatement(CREATEHUMAN);
            createOrUpHuman(human);
        } catch (SQLException e) {
            System.out.println("Fail, not created");
        }
    }

    @Override
    public void updateHuman(int id, Human human) {
        try {
            preparedStatement = connection.prepareStatement(UPDATEHUMAN);
            preparedStatement.setInt(9, id);
            createOrUpHuman(human);
            System.out.println(id + " - User up");
        } catch (SQLException e) {
            System.out.println("Can't up");
        }
    }

    private void createOrUpHuman(Human human) throws SQLException {
        preparedStatement.setString(1, human.getName());
        preparedStatement.setString(2, human.getLastName());
        preparedStatement.setString(3, human.getPatronymic());
        preparedStatement.setString(4, human.getCity());
        preparedStatement.setString(5, human.getStreet());
        preparedStatement.setString(6, human.getHouse());
        preparedStatement.setString(7, human.getFlat());
        preparedStatement.setString(8, human.getNumberPassport());
        preparedStatement.executeUpdate();
    }

    @Override
    public void deleteHuman(int id) {
        try {
            preparedStatement = connection.prepareStatement(DELETEHUMAN);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            System.out.println(id + " - User deleted");
        } catch (SQLException e) {
            System.out.println("Can't delete");
        }
    }
}
