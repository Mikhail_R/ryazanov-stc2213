package homework_13_JDBC;

public class Main {

    public static void main(String[] args) {

        ConnectToBD connectToBD = new ConnectToBD();
        CrudHumanRealise crudHumanRealise = new CrudHumanRealise();

        connectToBD.startGetConnection();

        crudHumanRealise.createHuman(new Human("Mark",
                "Antoninus",
                "Aurelius",
                "Тилимилитремдия",
                "st.Table",
                "30",
                "4",
                "2224"));
        crudHumanRealise.getHumanById(10);

        Human human = new Human("Mark",
                "Antoninus",
                "Aurelius",
                "Тилимилитремдия",
                "st.Table",
                "30",
                "4",
                "2225");
        crudHumanRealise.updateHuman(12, human);

        crudHumanRealise.getAllHumans();
        crudHumanRealise.deleteHuman(5);

        connectToBD.closeConnection();
    }
}

