package homework_13_JDBC;

import java.util.List;

public interface CrudHuman {

    List<Human> getAllHumans(); //Возвращает список всех людей из базы данных
    Human getHumanById(int id); //Возвращает конкретного человека, у которого определенный id
    void createHuman(Human human); //Создает человека и записывает его в БД
    void updateHuman(int id, Human human); //Обновляет данные по конкретному человеку
    void deleteHuman(int id); //Удаляет конкретного человека

}
