package homework_13_JDBC;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class ConnectToBD {
    static Connection connection;

    /* Попробовал сделать коннект через отдельно созданный файл,
    как показывал Олег на паре, обращаясь в него за данными (url, user, password) */

    public void startGetConnection() {
        Properties properties = new Properties();
        try {
            FileInputStream fileInputStream = new FileInputStream("Homeworks/src/main/resourses/db.properties");
            properties.load(fileInputStream);
        } catch (IOException e) {
            System.out.println("Error");
        }

        try {
            connection = DriverManager.getConnection(
                    properties.getProperty("URL"),
                    properties.getProperty("USER"),
                    properties.getProperty("PASS")
            );
        } catch (SQLException e) {
            System.out.println("Not connection");
        }
    }

    public void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Faild, try again close programm");
        }
    }
}

