package homework_06;

//        На вход подается последовательность чисел, стремящаяся к бесконечности
//        (последовательность может быть и из 100 чисел, может и их 1000000 чисел быть), оканчивающаяся на -101.
//        Необходимо вывести число, которое присутствует в последовательности минимальное количество раз.
//        Условия задачи:
//        - Все числа в диапазоне от -100 до 100.
//        - Числа встречаются не более 2 147 483 647-раз каждое.
//        - Cложность алгоритма - O(n)

import java.util.Random;

public class homeW_06 {
    public static void main(String[] args) {
        int[] array = new int[1000];

        randomDigitsInArray(array);
        printArray(array);
        findMinDigitRepeat(array);
    }

    public static void findMinDigitRepeat(int[] array) {
        int[] arrayNew = new int[201];
        int minDigit = array[0];
        int maxDigit = 2147483647;
        for (int i : array) {
            arrayNew[i + 100]++;
        }

        for (int i = 0; i < arrayNew.length; i++) {
            if (arrayNew[i] < maxDigit) {
                maxDigit = arrayNew[i];
                minDigit = i - 100;
            }
        }
        System.out.println("Наименее распространённое число в массиве: " + minDigit);
    }

    public static void randomDigitsInArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(201) - 100;
        }
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}
