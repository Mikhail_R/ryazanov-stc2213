package homework_11;

public class Main {
    public static void main(String[] args) {

        Human human = new Human("Petr",
                "Petrov",
                "Petrovich",
                "Moscow",
                "Moskovskaya",
                "1",
                "3",
                "AB7777777");
        Human human1 = new Human("Petra",
                "Petrova",
                "Petrovna",
                "Moscow",
                "Moskovskaya",
                "1",
                "3",
                "AB7777775");
        Human human2 = new Human("Petya",
                "Petrov",
                "Petrovich",
                "Moscow",
                "Moskovskaya",
                "1",
                "3",
                "AB7777776");

        System.out.println(human);
        System.out.println(human1);
        System.out.println(human2);
        System.out.println("Совпадает ли место проживания - " + human.getName() + " и " + human1.getName() + " = " + human.equals(human1));
        System.out.println("Совпадает ли место проживания - " + human1.getName() + " и " + human2.getName() + " = " + human1.equals(human2));
        System.out.println("Совпадает ли место проживания - " + human.getName() + " и " + human2.getName() + " = " + human.equals(human2));
    }
}
