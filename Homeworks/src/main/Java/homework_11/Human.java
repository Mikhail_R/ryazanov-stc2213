package homework_11;

import java.util.Objects;

public class Human {

    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;

    private String numberPassport;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    public Human(String name,
                 String lastName,
                 String patronymic,
                 String city,
                 String street,
                 String house,
                 String flat,
                 String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return city.equals(human.city)
                && street.equals(human.street)
                && house.equals(human.house)
                && flat.equals(human.flat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city,
                street,
                house,
                flat);
    }

    @Override
    public String toString() {
        return lastName + " " + name + " " + patronymic + " \n " +
                "Пасспорт: " + " \n " +
                "Серия: " + numberPassport + " \n " +
                "Город: " + city + ", ул. " + street + ", дом " + house + ", квартира " + flat;
    }
}
