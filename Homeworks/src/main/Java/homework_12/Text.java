package homework_12;

import java.util.Objects;

public class Text {
    private String stroke = "Маша и три медведя и снова Маша и опять Маша";
    private int a = 1;

    public String getStroke() {
        return stroke;
    }

    public void setStroke(String stroke) {
        this.stroke = stroke;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public Text(String stroke) {
        this.stroke = stroke;
    }

    public Text() {
    }
}
