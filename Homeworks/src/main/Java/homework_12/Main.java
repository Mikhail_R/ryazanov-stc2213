package homework_12;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map<String, Integer> manyWords = new HashMap<>();

        Text text = new Text();
        String str = text.getStroke();
        String[] words = str.split(" ");
        for (String word : words) {
            if (manyWords.containsKey(word)) {
                manyWords.put(word, manyWords.get(word) + 1);
            } else {
                manyWords.put(word, text.getA());
            }
        }
        System.out.println(manyWords);
    }
}
