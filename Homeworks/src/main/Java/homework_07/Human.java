package homework_07;

public class Human {
    private String name;
    private String lastname;
    private int age;

    public Human(String name, String lastname, int age) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Human (" +
                "name = " + name +
                ", lastname = " + lastname +
                ", age = " + age +
                ')';
    }

}
