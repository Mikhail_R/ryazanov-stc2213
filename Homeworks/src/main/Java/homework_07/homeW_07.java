package homework_07;

import java.util.Arrays;
import java.util.Random;

public class homeW_07 {
    public static void main(String[] args) {
        Random random = new Random();
        Human[] humans = new Human[random.nextInt(25) + 1];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human("Human" + i, "Hum" + i, random.nextInt(100) + 1);
        }
        System.out.println(Arrays.toString(humans));

        BubbleSort(humans);
        System.out.println(Arrays.toString(humans));

    }

    public static void BubbleSort(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            for (int j = 0; j < humans.length - 1; j++) {
                if (humans[j].getAge() > humans[j + 1].getAge()) {
                    int temp = humans[j].getAge();
                    humans[j].setAge(humans[j + 1].getAge());
                    humans[j + 1].setAge(temp);
                }
            }
        }
    }
}
