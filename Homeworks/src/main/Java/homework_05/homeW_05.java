package homework_05;

//        Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые

public class homeW_05 {
    public static void main(String[] args) {
        int[] Array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.print("Принимаемый массив: ");
        for (int i : Array) {
            System.out.print(i + " ");
        }
        System.out.println();
        replace(Array);
    }

    public static void replace(int[] Array) {
        int[] Array2 = new int[Array.length];
        System.out.print("Полученный массив: ");
        for (int i = 0; i < Array.length; i++) {
            if (Array[i] != 0) {
                Array2[i] = Array[i];
                System.out.print(Array2[i] + " ");
            }
        }
    }
}
