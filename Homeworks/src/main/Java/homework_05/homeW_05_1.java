package homework_05;

//    Реализовать функцию, принимающую на вход массив и целое число.
//    Данная функция должна вернуть индекс этого числа в массиве.
//    Если число в массиве отсутствует - вернуть -1.

public class homeW_05_1 {
    public static void main(String[] args) {
        int[] Array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int num = 18;
        System.out.print("Заданный массив: ");
        for (int i : Array) {
            System.out.print(i + " ");
        }
        System.out.println();
        numAndArr(Array, num);
    }

    public static int numAndArr(int[] Array, int num) {
        for (int i = 0; i < Array.length; i++) {
            if (Array[i] == num) {
                System.out.print(i + " - Номер индекса в массиве введённого числа ");
                return i;
            }
        }
        System.out.println(num + " - элемент отсутствует");
        return -1;
    }
}