package homework_09;

import java.util.Random;

public class Circle extends Elipse implements Moveable {
    Random rnd = new Random();

    public void move(int x, int y) {
        setX(x);
        setY(y);
        System.out.println("Перемещаем круг в координаты: " + "X: " + x + " Y: " + y);
    }

    @Override
    void getPerimeter() {
        setX(rnd.nextInt(200));
        int r = getX();
        double perimetrCircle = 2 * Math.PI * r;
        System.out.println("Периметр круга равен: " + perimetrCircle);
    }
}
