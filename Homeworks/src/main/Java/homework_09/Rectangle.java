package homework_09;

import java.util.Random;

public class Rectangle extends Figure {
    Random rnd = new Random();

    @Override
    void getPerimeter() {
        setX(rnd.nextInt(200));
        setY(rnd.nextInt(200));
        int a = getX(), b = getY();
        int perimetrRectangle = (a + b) * 2;
        System.out.println("Периметр прямоугольника равен: " + perimetrRectangle);
    }
}
