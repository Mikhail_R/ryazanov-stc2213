package homework_09;

import java.util.Random;

public class Elipse extends Figure {

    Random rnd = new Random();

    @Override
    void getPerimeter() {
        setX(rnd.nextInt(200));
        setY(rnd.nextInt(200));
        int a = getX(), b = getY();
        int abMinus = a - b, abSum = a + b;
        double perimetrElipse = 4 * (Math.PI * a * b + abMinus * abMinus) / abSum;
        System.out.println("Периметр элипса равен: " + perimetrElipse);
    }
}
