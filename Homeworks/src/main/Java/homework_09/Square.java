package homework_09;

import java.util.Random;

public class Square extends Rectangle implements Moveable {
    Random rnd = new Random();

    public void move(int x, int y) {
        setX(x);
        setY(y);
        System.out.println("Перемещаем квадрат в координаты: " + "X: " + x + " Y: " + y);
    }

    @Override
    void getPerimeter() {
        setX(rnd.nextInt(200));
        int a = getX();
        int perimetrSquare = 4 * a;
        System.out.println("Периметр квадрата равен: " + perimetrSquare);
    }
}
