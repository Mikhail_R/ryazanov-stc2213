package homework_09;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random rnd = new Random();

        Circle circle = new Circle();
        Square square = new Square();
        Rectangle rectangle = new Rectangle();
        Elipse elipse = new Elipse();

        Figure[] allFigure = {elipse, square, rectangle, circle};
        for (int i = 0; i < allFigure.length; i++) {
            allFigure[i].getPerimeter();
            System.out.print(allFigure[i]);
        }

        Moveable[] moveables = {circle, square};
        for (int i = 0; i < moveables.length; i++) {
            moveables[i].move(rnd.nextInt(200), rnd.nextInt(200));
            System.out.print(moveables[i]);
        }
    }
}

