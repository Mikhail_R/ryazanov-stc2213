package homework_08;

public class Designer extends Worker {

    public Designer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Я работаю как художник, потому что так вижу");
        System.out.println();
    }

    @Override
    public void goToVacation(int days) {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Я на удалёнке. Сейчас живу на острове уже: " + days + " дней");
        System.out.println();
    }
}
