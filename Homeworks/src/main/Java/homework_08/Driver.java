package homework_08;

public class Driver extends Worker {

    public Driver(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Если меня здесь нет, значит я на работе");
        System.out.println();
    }

    @Override
    public void goToVacation(int days) {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Уехал, буду когда вернусь." + "\n" + "Вероятно через: " + days + " дней");
        System.out.println();
    }
}
