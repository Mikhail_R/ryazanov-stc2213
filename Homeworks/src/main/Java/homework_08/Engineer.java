package homework_08;

public class Engineer extends Worker {

    public Engineer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Я работаю пока мне платят");
        System.out.println();
    }

    @Override
    public void goToVacation(int days) {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Меня не будет: " + days + " дней");
        System.out.println();
    }
}
