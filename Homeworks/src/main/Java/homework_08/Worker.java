package homework_08;


public class Worker {
    private String name;
    private String lastName;
    private String profession;

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public void goToWork() {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
        System.out.println("Я работаю, как шахтёр");
    }

    public void goToVacation(int days) {
        int howMatchDay = days;
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
        System.out.println("Меня не будет: " + days + " дней");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
