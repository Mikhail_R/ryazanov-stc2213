package homework_08;

public class Main {
    public static void main(String[] args) {

        Teacher teach = new Teacher("Марфа", "Васильевна", "Учитель");
        teach.goToWork();
        teach.goToVacation(20);

        Engineer en = new Engineer("Алексей", "Фёдорович", "Инженер");
        en.goToWork();
        en.goToVacation(7);

        Designer dis = new Designer("Павел", "Михайлович", "Дизайнер");
        dis.goToWork();
        dis.goToVacation(30);

        Driver drive = new Driver("Николай", "Петрович", "Водитель");
        drive.goToWork();
        drive.goToVacation(5);

    }
}
