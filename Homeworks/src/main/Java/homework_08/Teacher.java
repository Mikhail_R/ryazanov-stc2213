package homework_08;

public class Teacher extends Worker {

    public Teacher(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Я работаю c 8 до 17");
        System.out.println();
    }

    @Override
    public void goToVacation(int days) {
        System.out.print("Я: " + getName() + " " + getLastName() + " - " + getProfession() +
                         "\n" + "Меня не будет: " + days + " дней");
        System.out.println();
    }
}
