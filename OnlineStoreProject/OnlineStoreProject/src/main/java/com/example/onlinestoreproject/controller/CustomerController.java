package com.example.onlinestoreproject.controller;

import com.example.onlinestoreproject.model.Customer;
import com.example.onlinestoreproject.model.dto.CustomerDto;
import com.example.onlinestoreproject.servise.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/allcustomers")
    public List<Customer> getAllPerson() {
        List<Customer> customer = customerService.findAll();
        return customer;
    }

    @GetMapping("/{id}")
    public CustomerDto findCustomerById(@PathVariable Long id) {
        return customerService.findById(id);
    }

    @PostMapping("/createcustomer")
    public Customer createCustomer(@RequestBody CustomerDto customerDto) {
        return customerService.saveCustomer(customerDto);
    }

    @PutMapping("/updatecustomer")
    public Customer upCustomer(@RequestBody CustomerDto customerDto){
        return customerService.updateCustomer(customerDto);
    }

    @DeleteMapping("/deleteCustomer/{id}")
    public void deleteCustomer(@PathVariable Long id) {
        customerService.removeCustomer(id);
    }


}
