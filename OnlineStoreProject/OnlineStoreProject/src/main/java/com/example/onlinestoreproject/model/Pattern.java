package com.example.onlinestoreproject.model;

import javax.persistence.*;

@Entity
@Table(name = "leather_patterns")
public class Pattern {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    @Column(name = "image_link")
    private String imageLink;
    @Column(name = "file_link")
    private String fileLink;
    private Integer price;
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne
    @JoinColumn(name = "categories_id")
    private Categories categories;

    public Pattern() {
    }

    public Pattern(String title, String description, String imageLink, String fileLink, Integer price) {
        this.title = title;
        this.description = description;
        this.imageLink = imageLink;
        this.fileLink = fileLink;
        this.price = price;
    }

    public Pattern(Long id, String title, String description, String imageLink, String fileLink, Integer price) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageLink = imageLink;
        this.fileLink = fileLink;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getFileLink() {
        return fileLink;
    }

    public void setFileLink(String fileLink) {
        this.fileLink = fileLink;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }
}
