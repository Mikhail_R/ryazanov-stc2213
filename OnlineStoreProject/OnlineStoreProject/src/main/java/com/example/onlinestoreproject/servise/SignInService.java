package com.example.onlinestoreproject.servise;

import com.example.onlinestoreproject.model.dto.SignInForm;
import com.example.onlinestoreproject.model.dto.SignUpForm;

public interface SignInService {
    void signIn(SignInForm form);

}
