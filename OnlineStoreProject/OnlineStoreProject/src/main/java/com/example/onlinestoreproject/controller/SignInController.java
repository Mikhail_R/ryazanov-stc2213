package com.example.onlinestoreproject.controller;

import com.example.onlinestoreproject.model.dto.SignInForm;
import com.example.onlinestoreproject.servise.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class SignInController {

    private SignInService signInService;

    @Autowired
    public void setSignInController(SignInService signInService) {
        this.signInService = signInService;
    }

//    @GetMapping("/signIn")
//    public String getSignInPage(Authentication authentication) {
//        if (authentication != null) {
//            return "redirect:/";
//        }
//        return "signIn";
//    }

    @PostMapping("/signIn")
    public String signIn(@Valid SignInForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(form);
            return "signIn";
        }
        signInService.signIn(form);
        return "redirect:/signIn";
    }
}
