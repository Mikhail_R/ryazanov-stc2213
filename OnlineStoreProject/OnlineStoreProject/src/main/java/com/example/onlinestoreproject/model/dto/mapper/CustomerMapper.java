package com.example.onlinestoreproject.model.dto.mapper;

import com.example.onlinestoreproject.model.Customer;
import com.example.onlinestoreproject.model.dto.CustomerDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {

    CustomerMapper MAPPER = Mappers.getMapper(CustomerMapper.class);

    Customer toPerson(CustomerDto customerDto);

    @InheritInverseConfiguration
    CustomerDto toPersonDto(Customer customer);
}
