package com.example.onlinestoreproject.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

//"Правило" 1 - Пользователи приложения
@EnableWebSecurity
public class SecurityConfig {
    @Autowired
    public void authConfig(AuthenticationManagerBuilder auth,
                           CustomerAuthService customerAuthService,
                           PasswordEncoder passwordEncoder) throws Exception {
// Создание суперюзера(админ)
        auth.inMemoryAuthentication()
                .withUser("mem_user")
                .password(passwordEncoder.encode("password"))
                .roles("ADMIN");

// Вытаскивает пользователей из базы данных
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(customerAuthService);
        provider.setPasswordEncoder(passwordEncoder);
        auth.authenticationProvider(provider);
    }

//"Правило" 2 - Реакция приложения на пользователей, отображение страниц
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/pattern/**")
                .hasAnyRole("ADMIN", "USER")
                .antMatchers("/cart/**")
                .hasAnyRole("ADMIN", "USER")
                .antMatchers("/customer/**")
                .hasRole("ADMIN")
                .antMatchers("/login/**")
                .anonymous()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll()
                .and()
                .httpBasic()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED); // создаёт сеанс в случае необходимости
        return http.build();
    }

}
