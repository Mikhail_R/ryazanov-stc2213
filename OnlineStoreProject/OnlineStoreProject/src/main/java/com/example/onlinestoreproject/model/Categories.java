package com.example.onlinestoreproject.model;

import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
@Entity
@Table(name = "categories")
public class Categories {
    @Id
    private Integer id;
    private String title;
    @JsonIgnore
    @OneToMany(mappedBy = "categories")
    private List<Pattern> patterns;

}
