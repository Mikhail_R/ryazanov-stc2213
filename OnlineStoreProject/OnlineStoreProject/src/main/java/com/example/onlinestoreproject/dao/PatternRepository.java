package com.example.onlinestoreproject.dao;

import com.example.onlinestoreproject.model.Pattern;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface PatternRepository extends JpaRepository<Pattern, Long> {
    List<Pattern> findPatternsByPriceBefore(Integer price);



}
