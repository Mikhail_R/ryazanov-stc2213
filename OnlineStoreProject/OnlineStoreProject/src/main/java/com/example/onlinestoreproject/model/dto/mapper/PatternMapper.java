package com.example.onlinestoreproject.model.dto.mapper;

import com.example.onlinestoreproject.model.Pattern;
import com.example.onlinestoreproject.model.dto.PatternDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PatternMapper {

    PatternMapper MAPPER = Mappers.getMapper(PatternMapper.class);

    Pattern toPattern(PatternDto patternDto);

    @InheritInverseConfiguration
    PatternDto toPatternDto(Pattern pattern);

}
