package com.example.onlinestoreproject.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(
        value = {"classpath:onlineshop.properties"},
        ignoreResourceNotFound = true
)
public class AppConfig {

}