package com.example.onlinestoreproject.servise;

import com.example.onlinestoreproject.dao.CustomerRepository;
import com.example.onlinestoreproject.model.Customer;
import com.example.onlinestoreproject.model.dto.SignInForm;
import org.springframework.stereotype.Service;

@Service
public class SignInServiceImpl implements SignInService {


    private final CustomerRepository customerRepository;

    public SignInServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void signIn(SignInForm form) {
        Customer customer = new Customer(form.getLogin(), form.getPassword());
        customerRepository.save(customer);
    }
}
