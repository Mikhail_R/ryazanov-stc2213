package com.example.onlinestoreproject.controller;

import com.example.onlinestoreproject.model.Pattern;
import com.example.onlinestoreproject.model.dto.PatternDto;
import com.example.onlinestoreproject.servise.PatternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pattern")
public class PatternController {

    private PatternService patternService;

    @Autowired
    public void setPatternService(PatternService patternService){
        this.patternService = patternService;
    }

    @GetMapping("/allpatterns")
    public List<Pattern> getAllPatterns(){
        List<Pattern> patterns = patternService.findAll();
        return patterns;
    }

    @GetMapping("/allpatternsortprice")
    public List<Pattern> getAllPatternsWithSortPrice(){
        List<Pattern> patterns = patternService.findAllPatternsWithSortPrice();
        return patterns;
    }

    @GetMapping("/patternsbyprice/{price}")
    public List<Pattern> findPatternByPrice(@PathVariable Integer price){
        List<Pattern> patterns = patternService.findAllPatternByPrice(price);
        return patterns;
    }

    @GetMapping("/{id}")
    public PatternDto findPatternById(@PathVariable Long id) {
        return patternService.findById(id);
    }

    @PostMapping("/createpattern")
    public Pattern createPattern(@RequestBody PatternDto patternDto) {
        return patternService.savePattern(patternDto);
    }

    @PutMapping("/updatepattern")
    public Pattern upPattern(@RequestBody PatternDto patternDto){
        return patternService.updatePattern(patternDto);
    }

    @DeleteMapping("/deletepattern/{id}")
    public void deletePattern(@PathVariable Long id) {
        patternService.removePattern(id);
    }

}
