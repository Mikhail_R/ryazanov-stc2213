package com.example.onlinestoreproject.servise;

import com.example.onlinestoreproject.dao.PatternRepository;
import com.example.onlinestoreproject.model.Pattern;
import com.example.onlinestoreproject.model.dto.PatternDto;
import com.example.onlinestoreproject.model.dto.mapper.PatternMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
@Validated
public class PatternService {

    private PatternRepository patternRepository;

    @Autowired
    public void setPatternRepository(PatternRepository patternRepository) {
        this.patternRepository = patternRepository;
    }
// Метод показывает все товары
    public List<Pattern> findAll() {
        return patternRepository.findAll();
    }
// Сортирует товары по возрастанию цены
    public List<Pattern> findAllPatternsWithSortPrice() {
        return patternRepository.findAll(Sort.by("price").ascending());
    }

// Фильтр товаров в меньшую сторону по цене заданной в параметрах
    public List<Pattern> findAllPatternByPrice(Integer price) {
       return patternRepository.findPatternsByPriceBefore(price);

    }
// Метод нахождения товара по id
    public PatternDto findById(Long id) {
        Optional<Pattern> byId = patternRepository.findById(id);
        Pattern pattern = byId.orElseGet(Pattern::new);
        PatternDto patternDto = PatternMapper.MAPPER.toPatternDto(pattern);
        return patternDto;
    }

// Метод создаёт товар
    @Transactional
    public Pattern savePattern(@Valid PatternDto patternDto) {
        Pattern pattern = PatternMapper.MAPPER.toPattern(patternDto);
        return patternRepository.save(pattern);
    }
// Метод обновляет товар
    @Transactional
    public Pattern updatePattern(PatternDto patternDto) {
        Optional<Pattern> byId = patternRepository.findById(patternDto.getId());
        byId.orElseGet(Pattern::new);
        Pattern pattern = PatternMapper.MAPPER.toPattern(patternDto);
        return patternRepository.save(pattern);
    }
// Метод удаляет товар
    public void removePattern(Long id) {
        patternRepository.deleteById(id);
    }
}
