package com.example.onlinestoreproject.servise;

import com.example.onlinestoreproject.dao.CustomerRepository;
import com.example.onlinestoreproject.model.Customer;
import com.example.onlinestoreproject.model.dto.CustomerDto;
import com.example.onlinestoreproject.model.dto.mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
@Validated
public class CustomerService {
    private CustomerRepository customerRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public CustomerDto findById(Long id) {
        Optional<Customer> byId = customerRepository.findById(id);
        Customer customer = byId.orElseGet(Customer::new);
        CustomerDto customerDto = CustomerMapper.MAPPER.toPersonDto(customer);
        return customerDto;
    }

    @Transactional
    public Customer saveCustomer(@Valid CustomerDto customerDto) {
        customerDto.setPassword(passwordEncoder.encode(customerDto.getPassword()));
        Customer customer = CustomerMapper.MAPPER.toPerson(customerDto);
        return customerRepository.save(customer);
    }

    @Transactional
    public Customer updateCustomer(CustomerDto customerDto) {
        Optional<Customer> byId = customerRepository.findById(customerDto.getId());
        byId.orElseGet(Customer::new);
        customerDto.setPassword(passwordEncoder.encode(customerDto.getPassword()));
        Customer customer = CustomerMapper.MAPPER.toPerson(customerDto);
        return customerRepository.save(customer);

    }

    @Transactional
    public void removeCustomer(Long id) {
        customerRepository.deleteById(id);
    }
}
