CREATE TABLE leather_patterns
(
    id SERIAL PRIMARY KEY,
    title VARCHAR,
    description VARCHAR,
    image_link TEXT,
    file_link TEXT,
    price INT
);

INSERT INTO leather_patterns(title,
                             description,
                             image_link,
                             file_link,
                             price) VALUES('woven clutch bag',
                                           'Clutch bag sise: 23x13x4 cm',
                                           'https://www.hboy.store/product-page/leather-clutch-bag-pattern-pdf/image-clutch-bag1.jpeg',
                                           'https://www.hboy.store/product-page/leather-clutch-bag-pattern-pdf/file-clutch-bag.pdf',
                                           432);
INSERT INTO leather_patterns(title,
                             description,
                             image_link,
                             file_link,
                             price) VALUES('waist bag',
                                           'Bag sise: 36x15x8 cm',
                                           'https://www.hboy.store/product-page/leather-waist-bag-pattern-pdf/image-bag1.jpeg',
                                           'https://www.hboy.store/product-page/leather-waist-bag-pattern-pdf/file-bag.pdf',
                                           544);
INSERT INTO leather_patterns(title,
                             description,
                             image_link,
                             file_link,
                             price) VALUES('long wallet',
                                           'Clutch wallet sise: 21x11 cm',
                                           'https://www.hboy.store/product-page/leather-clutch-pattern-pdf/image-clutch1.jpeg',
                                           'https://www.hboy.store/product-page/leather-clutch-pattern-pdf/file-clutch.pdf',
                                           520);
INSERT INTO leather_patterns(title,
                             description,
                             image_link,
                             file_link,
                             price) VALUES('documents holder',
                                           'Holder sise: 14,5x9,5 cm',
                                           'https://www.hboy.store/product-page/leather-documents-holder-pattern-pdf/image-documents-holder1.jpeg',
                                           'https://www.hboy.store/product-page/leather-documents-holder-pattern-pdf/file-documents-holder.pdf',
                                           352);
--
CREATE TABLE categories
(
    id SERIAL PRIMARY KEY,
    title VARCHAR,
    patterns_id INT REFERENCES leather_patterns (id)
);

INSERT INTO categories(title) VALUES('wallets');
INSERT INTO categories(title) VALUES('bags');
INSERT INTO categories(title) VALUES('clutch bags');
INSERT INTO categories(title) VALUES('holders for documents');


CREATE TABLE customers
(
    id SERIAL PRIMARY KEY,
    login VARCHAR UNIQUE NOT NULL,
    name VARCHAR,
    last_name VARCHAR,
    email VARCHAR UNIQUE NOT NULL,
    password VARCHAR NOT NULL
);

INSERT INTO customers(login, name, last_name, email, password) VALUES('PaPav', 'Pavel', 'Pavlov', 'pavelp@gmail.com', 'password1');
INSERT INTO customers(login, name, last_name, email, password) VALUES('AlAlex', 'Alex', 'Alexandrov', 'alexa@gmail.com', 'password2');
INSERT INTO customers(login, name, last_name, email, password) VALUES('MaMar', 'Mark', 'Markov', 'markm@gmail.com', 'password3');
INSERT INTO customers(login, name, last_name, email, password) VALUES('BiBil', 'Bill', 'Billov', 'billb@gmail.com', 'password4');


CREATE TABLE orders
(
    id SERIAL PRIMARY KEY,
    patterns_id INT REFERENCES leather_patterns (id),
    customer_id INT REFERENCES customers (id),
    date_of_order TIMESTAMP
);

CREATE TABLE roles
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR
);

INSERT INTO roles(name) VALUES('ROLE_ADMIN'),
                               ('ROLE_USER'),
                               ('ROLE_VIEWER');

CREATE TABLE customers_roles
(
    id   SERIAL PRIMARY KEY,
    customer_id BIGINT,
    role_id BIGINT
);


