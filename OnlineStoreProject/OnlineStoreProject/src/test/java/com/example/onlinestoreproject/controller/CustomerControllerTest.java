package com.example.onlinestoreproject.controller;

import com.example.onlinestoreproject.dao.CustomerRepository;
import com.example.onlinestoreproject.model.Customer;
import com.example.onlinestoreproject.model.dto.CustomerDto;
import com.example.onlinestoreproject.servise.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CustomerControllerTest {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    private CustomerRepository customerRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    CustomerDto customerDto = new CustomerDto("NeRobot", "eeeee", "tttt", "ddd@fsaf.ru", "pass");


    @Test
    void getAllPerson() {
    }

    @Test
    void findCustomerById() {
        assertNotNull(customerService.findById(2L));
    }

    @Test
    void createCustomer() {
        customerService.saveCustomer(customerDto);
        assertNotNull(customerRepository.findCustomerByLogin("NeRobot"));
    }

    @Test
    void upCustomer() {
    }

    @Test
    void deleteCustomer() {
    }
}